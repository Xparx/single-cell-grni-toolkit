# This code is inspired by the method presented in
# [Batson and Royer, 2019](https://arxiv.org/abs/1901.11365)
import scipy as _sp
import scanpy as _sc
from scipy.sparse import issparse as _issparse, csr as _csr
from sklearn.utils.extmath import randomized_svd as _randomized_svd, safe_sparse_dot as _safe_sparse_dot
from sklearn.utils import check_array as _check_array
from sklearn.metrics import mean_squared_error as _mse, r2_score as _r2
from sklearn.model_selection import ShuffleSplit as _ShuffleSplit
from sklearn.decomposition import TruncatedSVD as _TruncatedSVD
# from sklearn.preprocessing import StandardScaler
# from scprocessing.preprocessing import noise2self


def normal_Xsplit(X, mean=0.5, sd=_sp.sqrt(0.25), low=0, upp=1):
    """Split the data as in noise2self in to one masked data and one target.

    :param X: ndarray, csr_matrix
    :returns: X_masked, X_target
    :rtype: ndarray, csr_matrix

    """

    from scipy.stats import truncnorm

    def get_truncated_normal(mean=0, sd=0.1, low=0, upp=1):
        return truncnorm((low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)

    gtn = get_truncated_normal(mean=mean, sd=sd, low=low, upp=upp)

    if _issparse(X):
        rn = gtn.rvs(X.data.shape)
        X_target = X.data * rn
        X_masked = X.data - X_target

        X_target = _csr.csr_matrix((X_target, X.nonzero()))
        X_masked = _csr.csr_matrix((X_masked, X.nonzero()))

    else:
        X_target = _sp.array([])
        for x in X:
            rn = gtn.rvs(x.shape)
            y = (x * rn).reshape((1, -1))
            if X_target.size == 0:
                X_target = y
            else:
                X_target = _sp.append(X_target, y, 0)

        X_masked = X - X_target

    return X_masked, X_target


def uniform_Xsplit(X, **kwargs):
    """Split the data as in noise2self in to one masked data and one target.

    :param X: ndarray, csr_matrix
    :returns: X_masked, X_target
    :rtype: ndarray, csr_matrix

    """

    from scipy.stats import uniform

    gtn = uniform()

    if _issparse(X):
        rn = gtn.rvs(X.data.shape)
        X_target = X.data * rn
        X_masked = X.data - X_target

        X_target = _csr.csr_matrix((X_target, X.nonzero()))
        X_masked = _csr.csr_matrix((X_masked, X.nonzero()))

    else:
        X_target = _sp.array([])
        for x in X:
            rn = gtn.rvs(x.shape)
            y = (x * rn).reshape((1, -1))
            if X_target.size == 0:
                X_target = y
            else:
                X_target = _sp.append(X_target, y, 0)

        X_masked = X - X_target

    return X_masked, X_target


def binomial_Xsplit(X, mean=0.5, **kwargs):
    """Split the data as in noise2self in to one masked data and one target.

    :param X: ndarray, csr_matrix
    :returns: X_masked, X_target
    :rtype: ndarray, csr_matrix

    """

    binom = _sp.random.binomial
    if _issparse(X):
        X_target = binom(X.data.astype(int), mean)

        X_masked = X.data - X_target

        X_target = _csr.csr_matrix((X_target, X.nonzero()))
        X_masked = _csr.csr_matrix((X_masked, X.nonzero()))

        X_target.eliminate_zeros()
        X_masked.eliminate_zeros()
    else:
        X_target = _sp.array([])
        for x in X:
            # rn = gtn.rvs(x.shape)
            y = (binom(x, mean)).reshape((1, -1))
            if X_target.size == 0:
                X_target = y
            else:
                X_target = _sp.append(X_target, y, 0)

        X_masked = X - X_target

    return X_masked, X_target


def rescale(*X, scaler=_sc.pp.scale, **sckwargs):

    return (scaler(x, **sckwargs) for x in X)
    # X_masked = _sc.pp.scale(X_masked.T, zero_center=False, copy=True).T
    # X_target = _sc.pp.scale(X_target.T, zero_center=False, copy=True).T


def calc_svd(X_train, n_comps, random_state=42):

    return _randomized_svd(X_train, n_components=n_comps, random_state=random_state)


def calc_decomposition(X_train, n_comps, decomp=_TruncatedSVD, random_state=42):

    return decomp(n_components=n_comps, random_state=random_state).fit(X_train)


def find_optimal_svd_components(X, Xsplit=uniform_Xsplit, n_comps=50, step=5, start=5, test_size=0.25, mean=0.5, sd=0.5, low=0, upp=1, random_state=42):

    X_masked, X_target = Xsplit(X, mean=mean, sd=sd, low=low, upp=upp)

    rs = _ShuffleSplit(1, test_size=test_size, random_state=random_state)

    train_index, test_index = next(rs.split(X_masked))

    U, S, V = calc_svd(X_masked[train_index, :], n_comps)

    mses = []
    r2s = []
    rank_range = range(start, n_comps, step)
    for k in rank_range:
        pinv = V[:k, :].T.dot(_sp.diag(1 / S[:k]).dot(U[:, :k].T))

        if _issparse(X_masked):
            predX = _csr.csr_matrix.dot(X_masked[test_index, :], _csr.csr_matrix(pinv))
            prediction = _csr.csr_matrix.dot(predX, X_target[train_index, :]).astype(_sp.float32)

            current_mse = _mse(prediction.toarray(), X_target[test_index, :].toarray())
            current_r2 = _r2(prediction.toarray(), X_target[test_index, :].toarray())

        else:
            predX = X_masked[test_index, :].dot(pinv)
            prediction = predX.dot(X_target[train_index, :])

            current_mse = _mse(prediction, X_target[test_index, :])
            current_r2 = _r2(prediction, X_target[test_index, :])

        mses.append(current_mse)
        r2s.append(current_r2)

    return mses, r2s, rank_range


def find_optimal_npcs(X, decomp=_TruncatedSVD, n_comps=50, step=5, start=5, test_size=0.25, mean=0.5, sd=None, low=0, upp=1, random_state=42):

    if sd is None:
        sd = _sp.sqrt(mean * (1 - mean))

    islist = False
    if isinstance(n_comps, (list, _sp.ndarray)):
        rank_range = n_comps
        islist = True
    else:
        rank_range = _sp.arange(start, n_comps, step)

    X_masked, X_target = uniform_Xsplit(X, mean=mean, sd=sd, low=low, upp=upp)

    X_masked = rescale(X_masked.T, scaler=_sc.pp.scale, zero_center=True, copy=True).T
    X_target = rescale(X_target.T, scaler=_sc.pp.scale, zero_center=True, copy=True).T

    rs = _ShuffleSplit(1, test_size=test_size, random_state=random_state)
    train_index, test_index = next(rs.split(X_masked))
    if islist:
        pca = calc_decomposition(X_masked[train_index, :], max(n_comps), decomp=decomp, random_state=random_state)
    else:
        pca = calc_decomposition(X_masked[train_index, :], n_comps, decomp=decomp, random_state=random_state)

    mses = []
    r2s = []

    for k in rank_range:

        Xtest = _check_array(X_masked[test_index, :], accept_sparse='csr')
        prediction = _sp.dot(_safe_sparse_dot(Xtest, pca.components_[:k, :].T), pca.components_[:k, :])

        # print(_issparse(X_target))
        current_mse = _mse(X_target[test_index, :].toarray() if _issparse(X_target) else X_target[test_index, :], prediction)
        mses.append(current_mse)

        current_r2 = _r2(X_target[test_index, :].toarray() if _issparse(X_target) else X_target[test_index, :], prediction)
        r2s.append(current_r2)

    return rank_range, mses, r2s


def run_pc_iteration(X_masked, X_target, decomp=_TruncatedSVD, n_comps=50, start=5, step=5, test_size=0.25, random_state=42):

    islist = False
    if isinstance(n_comps, (list, _sp.ndarray)):
        rank_range = n_comps
        islist = True
    else:
        rank_range = _sp.arange(start, n_comps, step)

    rs = _ShuffleSplit(1, test_size=test_size, random_state=random_state)
    train_index, test_index = next(rs.split(X_masked))
    if islist:
        pca = calc_decomposition(X_masked[train_index, :], max(n_comps), decomp=decomp, random_state=random_state)
    else:
        pca = calc_decomposition(X_masked[train_index, :], n_comps, decomp=decomp, random_state=random_state)

    mses = []
    r2s = []

    for k in rank_range:

        Xtest = _check_array(X_masked[test_index, :], accept_sparse='csr')
        prediction = _sp.dot(_safe_sparse_dot(Xtest, pca.components_[:k, :].T), pca.components_[:k, :])

        # print(_issparse(X_target))
        current_mse = _mse(X_target[test_index, :].toarray() if _issparse(X_target) else X_target[test_index, :], prediction)
        mses.append(current_mse)

        current_r2 = _r2(X_target[test_index, :].toarray() if _issparse(X_target) else X_target[test_index, :], prediction)
        r2s.append(current_r2)

    return rank_range, mses, r2s, pca
