import matplotlib as _mpl
import numpy as _np


def add_dots_legend(ax, sizes, labels=None, title='', fontsize=12, ncols=None, loc=8):
    '''https://blogs.oii.ox.ac.uk/bright/2014/08/12/point-size-legends-in-matplotlib-and-basemap-plots/'''
    lbs = []
    pltax = []
    for s in sizes:
        scax = ax.scatter([], [], s=s, edgecolors='none')

        if labels is None:
            lbs.append(str(s))

        pltax.append(scax)

    if labels is None:
        labels = lbs

    # labels = ["10", "50", "100", "200"]

    if ncols is None:
        ncols = len(labels)

    leg = ax.legend(pltax, labels, ncol=ncols, frameon=True, fontsize=fontsize,
                    handlelength=2, loc=loc, borderpad=1.8,
                    handletextpad=1, title=title, scatterpoints=1)

    return leg


class MidpointNormalize(_mpl.colors.Normalize):
    """
    Normalise the colorbar so that diverging bars work there way either side from a prescribed midpoint value)

    e.g. im=ax1.imshow(array, norm=MidpointNormalize(midpoint=0.,vmin=-100, vmax=100))
    """
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        _mpl.colors.Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return _np.ma.masked_array(_np.interp(value, x, y), _np.isnan(value))
