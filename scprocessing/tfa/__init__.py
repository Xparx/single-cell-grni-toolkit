import pandas as _pd
import numpy as _np
import scipy as _sp
from sklearn.linear_model import LinearRegression
from sklearn.utils import as_float_array, check_X_y, check_array
from time import time
import scanpy as sc
from ..datasets.utils import kh_ky_mapping
import warnings


def merge_duplicated_TFs(df, idsep=';'):
    """accepts a pandas DataFrame and merges duplicated rows and concatenate the indices"""

    dupdf = df[df.duplicated(keep=False)]
    if dupdf.shape[0] == 0:
        print('No redundancy among rows found. returning input.')
        return df

    dupdfix = dupdf.index.values

    merged = []
    for k, val in dupdf.groupby(dupdf.columns.tolist()):
        inx = val.index.tolist()
        i = idsep.join(inx)
        v = val.max(0)
        v.name = i
        merged.append(v)

    df = df.drop(dupdfix)
    df = _pd.concat([df, _pd.concat(merged, 1).T])

    return df


def BIC(X, y, coef, criteria='bic'):
    """Compute the Bayesian information criterion"""

    X, y = check_X_y(X, y, y_numeric=True)

    n_samples = X.shape[0]

    if criteria == 'aic':
        K = 2  # AIC
    elif criteria == 'bic':
        K = _np.log(n_samples)  # BIC
    else:
        raise ValueError('criterion should be either bic or aic')

    df = _np.sum(_np.abs(coef) > _np.finfo(coef.dtype).eps)
    if not hasattr(coef, '__iter__'):
        coef = _np.ones(X.shape[1]) * coef

    ypred = _np.dot(X, coef)
    e = y - ypred
    mean_squared_error = e.dot(e) / e.shape[0]  # residuals
    # mean_squared_error = _np.mean(R2)

    sigma2 = _np.var(y)

    eps64 = _np.finfo('float64').eps
    bi_ = (n_samples * mean_squared_error / (sigma2 + eps64) + K * df)  # Eqns. 2.15--16 in (Zou et al, 2007)

    # n_best = _np.argmin(self.criterion_)
    return bi_


def compute_tfa(prior, adata, layer='TFE', gene_set=None, method=LinearRegression, method_kw={'fit_intercept': False}, priorcol='link', scalenorm=True):
    """helper function for computing TFA.
    prior: is a link list with first column = TFs, second = Gene ID, and third = link.
    adata: is a sc.AnnData object with expression.
    layer: is the sc.AnnData layer to use.
    """
    from sklearn.preprocessing import scale

    P = prior.set_index(['TF', 'TARGET'])[priorcol].unstack()
    if gene_set is not None:
        if isinstance(gene_set, str):
            if gene_set == 'random':
                rank = 0
                while rank < min(P.shape):
                    print('Generating random gene set')
                    gene_set = adata.var.index.values[_np.random.randint(0, adata.var.shape[0], adata.var.shape[0] // 10)]
                    Ngenes = P.columns[P.columns.isin(gene_set)]
                    P = P[Ngenes].replace(_np.nan, 0).T
                    rank = _np.linalg.matrix_rank(P)
        else:
            Ngenes = P.columns[P.columns.isin(gene_set)]
            P = P[Ngenes].replace(_np.nan, 0).T

        __ = adata[:, adata.var[adata.var.index.isin(Ngenes)].index]

    else:
        __ = adata.copy()

    X = _pd.DataFrame(scale(__.layers[layer].copy()) if scalenorm else __.layers[layer].copy(), index=adata.obs_names, columns=__.var.index).T
    del __

    fitm = method(**method_kw)
    tl = _np.inf

    P = P.replace(_np.nan, 0).T
    TFA = {}

    for j, i in enumerate(X):
        print(f"TFA: working on {i}, progress {j/X.shape[1]*100:.2f}%, estimated time left: {tl* (X.shape[1]-j)/60:.2f}min", end='\r')

        ts = time()
        fitm.fit(P, X[i])
        # r2 = lstsq.score(P, X.T[i])
        TFA[i] = fitm.coef_.copy()
        tl = time() - ts

    TFA = _pd.DataFrame(TFA)

    TFA.index = P.columns
    TFA = sc.AnnData(TFA.T)

    return TFA


def compute_grn(TFA, TFE, zetavec, model=LinearRegression, model_kw={'fit_intercept': False}, prior=None, model_param='alpha', criteria='bic'):
    """Helper function for inferring a GRN through model selection.
    TFE: Transcription factor effect (estimated expression response to TFA).
    TFA: Transcription factor activity.
    prior: prior known TF --> Target connection (long form).
    use_prior: limit connections to prior network. Default: False.
    """

    # Y = _pd.DataFrame(TFE.layers['TFE'], index=TFE.obs_names, columns=TFE.var_names).copy()
    # X = TFA.X.copy()

    GRN = {}
    OBJ = {}
    tl = _np.inf
    for i, g in enumerate(TFE):

        print(f"working on {g}, progress {i/TFE.shape[1]*100:.2f}%, estimated time left: {tl* (TFE.shape[1]-i)/60:.2f}min", end='\r')
        ts = time()

        opt_model, objective = iterate_models(TFA, TFE[g], model, zetavec, model_kw=model_kw, model_param=model_param, criteria=criteria)

        GRN[g] = opt_model.coef_
        OBJ[g] = objective
        tl = time() - ts

    GRN = _pd.DataFrame(GRN)
    OBJ = _pd.DataFrame(OBJ)

    return GRN, OBJ


def compute_omp_bic(TFA, TFE, zetavec=200, model_kw={'fit_intercept': False}, prior=None, model_param='alpha', criteria='bic'):
    """Helper function for inferring a GRN through model selection.
    TFE: Transcription factor effect (estimated expression response to TFA).
    TFA: Transcription factor activity.
    prior: prior known TF --> Target connection (long form).
    use_prior: limit connections to prior network. Default: False.
    """
    from sklearn.linear_model import orthogonal_mp


    # Y = _pd.DataFrame(TFE.layers['TFE'], index=TFE.obs_names, columns=TFE.var_names).copy()
    # X = TFA.X.copy()

    GRN = {}
    OBJ = {}
    tl = _np.inf
    BICvals = []
    GRN = _sp.sparse.lil_matrix((TFE.shape[1], TFA.shape[1]))

    for i, g in enumerate(TFE):

        print(f"working on {g}, progress {i/TFE.shape[1]*100:.2f}%, estimated time left: {tl* (TFE.shape[1]-i)/60:.2f}min", end='\r')
        ts = time()

        # opt_model, objective = iterate_models(TFA, TFE[g], model, zetavec, model_kw=model_kw, model_param=model_param, criteria=criteria)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            coef = orthogonal_mp(TFA, TFE[g].values, n_nonzero_coefs=min(zetavec, TFA.shape[1]), return_path=True)

        # print(coef.T.shape)
        # print(coef.T)
        # print(min(zetavec, TFA.shape[1]))
        # print(g)
        # print(TFA.shape)
        # print(TFE[g].shape)
        bic = [BIC(TFA, TFE[g], cf, criteria=criteria) for cf in coef.T]
        optind = _np.argmin(bic)
        # print(bic)
        # print(optind)
        # print(_np.where(coef[:, optind]))
        if len(coef.shape) > 1:
            valind = _np.where(coef[:, optind])[0]
            vals = coef[valind, optind]
            GRN[i, valind] = vals
            BICvals.append(bic)
        else:
            vals = coef[optind]
            GRN[i, optind] = vals  # if one dimensional optind same as valind
            BICvals.append(bic)

        # GRN[g] = opt_model.coef_
        # OBJ[g] = objective
        tl = time() - ts

    GRN = GRN.tocsr()
    GRN = _pd.DataFrame(GRN.A)
    OBJ = _pd.DataFrame(OBJ)

    return GRN, OBJ


def iterate_models(X, y, model, zetavec, model_param='alpha', model_kw={'fit_intercept': False}, criteria='bic'):
    """iterate model over input parameters zetavec
    X: independent variable.
    y: dependant variable
    model: model to be fitted.
    zetavec: model-parameters to iterate.
    """

    BICS = []
    bic_min = _np.inf
    opt_model = None
    opt_zeta = None
    # fitm = model(**model_kw)
    if zetavec is not None:
        for zeta in zetavec:
            model_kw[model_param] = zeta
            fitm = model(**model_kw)
            # setattr(fitm, model_param, zeta)
            fitm.fit(X, y)
            bic_score = BIC(X, y, fitm.coef_, criteria=criteria)
            BICS.append(bic_score)

            # print(bic_score)
            if bic_min > bic_score:
                bic_min = bic_score
                opt_model = fitm
                opt_zeta = zeta

    else:
        opt_model = model(**model_kw)
        # setattr(fitm, model_param, zeta)
        opt_model.fit(X, y)
        BICS = []

    return opt_model, BICS


def convert_ids(network, replace_col, idmap=kh_ky_mapping, mapto='KHname'):

    IDS = _pd.read_csv(kh_ky_mapping, sep='\t', index_col=None)
    all_ids = network[replace_col].unique()
    TFS = []
    ky2019 = 'KY2019:' + IDS['KY2019']
    for tfs in all_ids:

        trans = []
        for tf in tfs.split(';'):
            # ky2019
            ss = IDS.loc[ky2019[ky2019 == tf].index].size
            if ss > 0:
                trans.append(IDS.loc[ky2019[ky2019 == tf].index[0]][mapto])
            else:
                trans.append(tf)

        TFS.append(";".join(_np.unique(trans)))

    TFS = _pd.Series(TFS)
    TFS.index = all_ids

    network[f'{replace_col}_Name'] = network[replace_col].replace(TFS)


def convert_grn2long(GRN, columns=['TF', 'TARGET', 'weight']):

    GRN_stack = GRN.replace(0, _np.nan).stack().reset_index()  # .plot.hist(bins=1000)
    if columns is not None:
        GRN_stack.columns = columns

    GRN_stack['abs_weight'] = _np.abs(GRN_stack['weight'])
    GRN_stack = GRN_stack.sort_values('abs_weight', ascending=False).sort_values(['TF', 'TARGET'], ascending=True)

    return GRN_stack


def merge_grn_with_gs(GRN, GS, merge_index=['TF', 'TARGET'], links='link', copy=True):

    # GS[index[-2]] = GS['TF'].replace(GRN.set_index('TF')['TF_Name'].drop_duplicates())
    # GS[index[-1]] = GS['KY_ID'].replace(GRN.set_index('KY_ID')['TARGET_Name'].drop_duplicates())

    GS = GS[GS['TARGET'].isin(GRN['TARGET'].unique())].copy()

    GS = GS.reset_index().set_index(merge_index)[links].astype(bool).astype(int)
    # GS.name = 'GS'

    overlap = GRN.copy() if copy else GRN
    overlap = overlap.reset_index().set_index(merge_index)
    overlap['exist'] = overlap['abs_weight'].replace(_np.nan, 0).astype(bool).astype(int)

    # overlap = overlap.merge(GS, left_index=True, right_index=True, how='outer')
    # print(overlap.head())
    # print(GS.head())
    overlap = overlap.merge(GS, left_on=merge_index, right_on=merge_index, how='outer')

    if 'index' in overlap.columns:
        del overlap['index']

    overlap = overlap.reset_index()

    return overlap


def compute_network_overlap_metrics(network, network_names):

    import itertools

    network = network[network_names].replace(_np.nan, 0).copy()

    pairs = itertools.combinations(network_names, 2)
    mcc = []
    for pair in pairs:
        tab = _pd.crosstab(network[pair[0]].astype(bool), network[pair[1]].astype(bool))

        if _np.prod(tab.shape) < 4:
            mcc.append(0)

        else:
            nom = tab[True][True] * tab[False][False] - tab[True][False] * tab[False][True]
            den = _np.sqrt((tab[True][True] + tab[True][False]) * (tab[True][True] + tab[False][True]) * (tab[False][False] + tab[True][False]) * (tab[False][False] + tab[False][True]))
            mcc.append(nom / den if den > 0 else 0)

    return mcc


def add_sign2prior(prior, fc_data):

    Index = ['TF', 'TARGET']
    orig_index = _pd.Index(prior[Index])
    __ = _np.outer(fc_data['log2FoldChange'], fc_data['log2FoldChange'])
    fc_data = _pd.DataFrame(__, index=fc_data['KY2019'], columns=fc_data['KY2019'])

    fc_data.columns.name = 'TF'
    fc_data = _np.sign(fc_data.stack()).reset_index()

    fc_data.columns = ['TF', 'TARGET', 'sign']
    prior = prior.merge(fc_data, left_on=Index, right_on=Index, how='left')

    prior = prior.set_index(Index)
    prior.loc[orig_index]

    prior['sign'] = prior['sign'].replace(_np.nan, 1)
    prior = prior.reset_index()
    prior = prior.drop_duplicates()
    prior = prior[~prior[Index].duplicated()]
    # del prior['index']

    return prior


def compute_pcheck(prior, adata, idth=_np.inf, odth=0.2, layer=None, min_weight=1e-10, pcheckcomp=True, method='Ridge', method_kw={'fit_intercept': False}):

    import sklearn.linear_model as LM

    if method is None:
        alg = LM.Ridge
    elif isinstance(method, str):
        alg = getattr(LM, method)
    else:
        alg = method

    if layer == 'X' or (layer is None):
        TFE = _pd.DataFrame(adata.X.copy(), index=adata.obs_names, columns=adata.var_names)
    else:
        TFE = _pd.DataFrame(adata.layers[layer].copy(), index=adata.obs_names, columns=adata.var_names)

    prior = prior[prior.sum(1) < idth].astype(int)
    prior = prior.loc[:, prior.sum(0) < odth * prior.shape[0]].astype(int)

    prior = prior[prior.sum(1) != 0]
    prior = merge_duplicated_TFs(prior.T).T

    if not pcheckcomp:
        prior.columns.name = 'TF'
        prior = prior[(prior.astype(bool).sum(1) != 0)]
        prior_long = prior.replace(0, _np.nan).stack().reset_index()
        prior_long.columns = ['TARGET', 'TF', 'link']
        return prior_long

    Q = prior.dot(prior.T)
    Qhat = (Q - _np.diag(_np.diag(Q))).astype(bool).astype(float)

    BICvals = []
    Qcheck = _sp.sparse.lil_matrix(Qhat.shape)
    for i, target in enumerate(Qhat.index):
        print(f'Pcheck on: {target}, n {100*i/Qhat.index.shape[0]:.2f}%', end='\r')

        X = TFE[Qhat.index[Qhat.loc[target] > 0]].to_numpy()
        y = TFE[target].to_numpy()

        if hasattr(alg, 'fit'):
            alg = alg(**method_kw)
            coef = alg.fit(X, y).coef_
        else:
            # coef = alg(X, y, n_nonzero_coefs=min(200, X.shape[1]), return_path=True)
            coef = alg(X, y, **method_kw)

        if len(coef.shape) > 1:
            bic = [BIC(X, y, cf) for cf in coef.T]
            optind = _np.argmin(bic)
            valind = _np.where(coef[:, optind])[0]
            vals = coef[valind, optind]
            Qcheck[i, valind] = vals
            BICvals.append(bic)
        else:
            Qcheck[i, _np.where(Qhat.loc[target] > 0)[0]]

    Qcheck = Qcheck.tocsr()

    # Qcheck.data.shape[0] / np.prod(Qcheck.shape)

    Pcheck = _np.multiply(Qcheck.dot(prior), prior)
    Pcheck[Pcheck.abs() < min_weight] = 0

    Pcheck.columns.name = 'TF'
    Pcheck = Pcheck[(Pcheck.astype(bool).sum(1) != 0)]
    pcheck_long = Pcheck.replace(0, _np.nan).stack().reset_index()
    pcheck_long.columns = ['TARGET', 'TF', 'link']

    return pcheck_long


def grn_pipeline(prior, adata, layer=None, pcheck_kw={'idth': _np.inf, 'odth': 0.2, 'min_weight': 1e-10}, markergenes=['FOXF1/2', 'MSGN1', 'DDR/1/2', 'NKX2-3'], zetavec=range(1, 100), model_kw={'fit_intercept': False, 'normalize': False}, criteria='bic', verbose=True, pcheckcomp=True):
    # from sklearn.linear_model import OrthogonalMatchingPursuit
    from sklearn.linear_model import LinearRegression

    if verbose:
        print('computing weighted prior')
        print(f'Shape of input prior P {prior.shape}, Shape of input data(Expr) {adata.shape}')

    pcheck = compute_pcheck(prior, adata, layer=layer, **pcheck_kw, pcheckcomp=pcheckcomp)

    tmp = adata[:, pcheck['TARGET'].unique()]
    if verbose:
        print('Computing TFA')
        print(f'Shape of input prior Pcheck {pcheck.shape}, Shape of input filter data {tmp.shape}')

    TFA = compute_tfa(pcheck, tmp, layer=layer, gene_set=None, method=LinearRegression)

    TFA.obsm = adata.obsm
    TFA.obs = adata.obs
    TFA.uns = adata.uns

    # if verbose:
    #     return pcheck, TFA

    if layer is None or (layer == 'X'):
        Y = _pd.DataFrame(adata.X, index=adata.obs_names, columns=adata.var_names).copy()
    else:
        Y = _pd.DataFrame(adata.layers[layer], index=adata.obs_names, columns=adata.var_names).copy()

    if markergenes is not None:
        markers = adata.var_names[adata.var['Gene Name'].isin(markergenes)]
        Y = Y[markers]

    X = TFA.X.copy()

    # GRN, BIC = compute_grn(X, Y, zetavec, model=OrthogonalMatchingPursuit, model_param='n_nonzero_coefs', model_kw=model_kw, criteria='aic')
    if verbose:
        print('Computing GRN')
        print(f'Shape of TFA(X) {X.shape}, Shape of Targets(Y) {Y.shape}')

    GRN, BIC = compute_omp_bic(X, Y, zetavec, model_param='n_nonzero_coefs', model_kw=model_kw, criteria=criteria)

    GRN.columns = TFA.var_names

    GRN.index = Y.columns
    GRN_stack = convert_grn2long(GRN.T)

    return GRN_stack, TFA


def fun(args):
    pass

