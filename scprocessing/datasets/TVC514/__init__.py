import os as _os
import pandas as _pd
import numpy as _np
# import ..utils as utils
from ..utils import get_ciona_genenames, add_annotation_to_data_var
from ..utils import *


_datadir = _os.path.join("..", "data", "TVC514", "filtered_feature_bc_matrix")
_hpfmap = _os.path.join("..", "data", "TVC514", "Sample_collection_barcoding.csv")


def load_counts(datapath=_datadir, map_genenames=True):

    import scanpy as sc

    adata = sc.read_10x_mtx(datapath)

    adata.obs.index.name = "barcode"

    if 'feature_types' in adata.var.columns:
        del adata.var['feature_types']

    adata.var.columns = ["Gene ID"]

    if map_genenames:
        genenames = get_ciona_genenames()
        add_annotation_to_data_var(adata, genenames)

    return adata


def convert_sbc_to_hpf(sbc_adata, hpfmap=_hpfmap, returnmap=False):

    sbcadata = sbc_adata.copy()
    df = _pd.read_csv(hpfmap, sep='\t')

    ind1 = df[['hpf', 'Index1']]
    ind1.columns = ['hpf', 'Gene ID']
    ind2 = df[['hpf', 'Index2']]
    ind2.columns = ['hpf', 'Gene ID']

    df = _pd.concat([ind1, ind2])

    df = df.set_index('Gene ID')
    df = sbcadata.var.merge(df, left_index=True, right_index=True)

    df['hpf'] = df['hpf'].astype(str)

    if returnmap:
        return df

    sbcadata.var['hpf'] = df['hpf']
    sbcadata.var = sbcadata.var.set_index('hpf')

    sbcadata = sbcadata.to_df().T
    sbcadata = sbcadata.groupby(sbcadata.index).sum().T
    return sbcadata


def find_reliable_sbc(sbcdf, cutof=0.5, min_count=1):

    def find_dominating(sbcdata, cutof, min_count):

        for x in X:
            if max(x) > min_count:
                x = x / sum(x)
                yield x.max() > cutof
            else:
                yield False

    X = sbcdf.values
    # sbcdf['hpf_reliable'] = '-1'

    morethan = [i for i in find_dominating(X, cutof, min_count)]
    # max_hpf = sbcdf.apply(_np.argmax, 1).values
    max_hpf = sbcdf.idxmax(1).values

    sbcdf['condition_reliable'] = [mh if mt else '-1' for mt, mh in zip(morethan, max_hpf)]

    return sbcdf['condition_reliable']
    # relible_cells = sbcdata.obs_names[morethan].copy()

    # adata.obs['sbc_group_reliable'].loc[relible_cells] = adata.obs['sbc_group'].loc[relible_cells].copy()
    # nonthere = adata.obs[(adata.obs['sbc_group_reliable'] == 'SBC000')].index
    # adata.obs['sbc_group_reliable'].loc[nonthere] = '-1'
    # adata.obs['sbc_group_reliable'] = adata.obs['sbc_group_reliable'].astype('category')
