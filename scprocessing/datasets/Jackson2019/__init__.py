import pandas as _pd
from scipy.sparse import csr_matrix as _csr_matrix
# from ..utils import add_annotation_to_data_var, get_ciona_genenames


_datasetfile = "../data/Jackson2019/SD1/data/103118_SS_Data.tsv.gz"
_geneannotationfile = "../data/Jackson2019/SD1/data/go_slim_mapping.tab"
_genenamefile = "../data/Jackson2019/SD1/data/yeast_gene_names.tsv"

_metadata = ["Genotype", "Genotype_Group", "Replicate", "Condition", "tenXBarcode"]


def load_Jackson2019(lf=_datasetfile, gnf=_genenamefile):

    import scanpy as sc

    adata = _pd.read_csv(lf, sep='\t', index_col=0)

    obs = adata[_metadata]

    for i in _metadata:
        del adata[i]

    genenames = load_gene_names(gnf)

    genenames = genenames.set_index("SystematicName")
    adata.columns.name = "SystematicName"

    adata = sc.AnnData(_csr_matrix(adata.values), var=adata.columns, obs=obs)
    var = adata.var.merge(genenames, how='left', on='SystematicName')

    var.loc[var['Name'].isna(), 'Name'] = var.loc[var['Name'].isna(), "SystematicName"].copy()
    adata.var = var.set_index('SystematicName')

    return adata


def load_go_terms(gaf=_geneannotationfile, grabfor=None, return_genes=True):
    """Load Yeast go terms.

    :param gaf: file location for the annotations.
    :param grabfor: string, if startswith("GO:") will fetch those go terms if other string will grab for the string in GO annotation.
    :param return_genes: if grabfor is not None and return_genes is True then a list of genes is returned.
    :returns:
    :rtype: pandas.DataFrame, or list

    """

    goterms = _pd.read_csv(gaf, sep='\t', header=None)

    if grabfor is not None:
        if grabfor.startswith("GO:"):
            genelist = goterms.loc[goterms[5].isin([grabfor]), 0].tolist()
        else:
            genelist = goterms.loc[[grabfor in i for i in goterms[4]], 0].tolist()

        if return_genes:
            goterms = genelist

    return goterms


def load_gene_names(gnf=_genenamefile):

    genenames = _pd.read_csv(gnf, sep='\t')

    genenames['Name'].isna()

    genenames.loc[genenames['Name'].isna(), 'Name'] = genenames.loc[genenames['Name'].isna(), "SystematicName"].copy()
    return genenames
