import os as _os
import scanpy as _sc
from ..utils import add_annotation_to_data_var, get_ciona_genenames
from ..utils import *
from ..TVC514 import find_reliable_sbc
import pandas as _pd
import glob as _glob

_datadir = _os.path.join('..', 'data', 'actseq', 'dpErk165')
_sbcmap = _os.path.join('..', 'data', 'actseq', 'SamBaMu_index.xlsx')


def load_counts(countdir=_os.path.join(_datadir, 'filtered_feature_bc_matrix')):

    adata = _sc.read_10x_mtx(countdir)
    genenames = get_ciona_genenames()

    # del adata.var['feature_types']
    adata = adata.copy()
    adata.obs.index.name = "barcode"

    adata.var.columns = ['Gene ID', 'feature_types']
    # adata.var.columns = ['Gene ID']

    add_annotation_to_data_var(adata, genenames)

    return adata


def load_HTOcounts(countdir=_os.path.join(_datadir, "HTOcount", "umi_count"), feature2use='HTO2-ACATGTTACCGT'):

    barcodes = _pd.read_csv(_os.path.join(countdir, 'barcodes.tsv.gz'), sep='\t', header=None)
    features = _pd.read_csv(_os.path.join(countdir, 'features.tsv.gz'), sep='\t', header=None)

    adata = _sc.read_mtx(_os.path.join(countdir, "matrix.mtx.gz"))
    adata = adata.T.copy()
    adata.var_names = features[0].values
    adata.obs_names = barcodes[0].values

    # adata.obs.index.name = "barcode"

    adata = adata[:, adata.var.index == feature2use].copy()

    # filterindes = _sc.pp.filter_cells(adata, min_counts=1, inplace=False)

    adata = adata[adata.X > 0].copy()

    return adata


def convert_sbc_to_hpf(sbc_adata, sbcmap=_sbcmap, returnmap=False, sheet_name='Ef1atagBFP_Panel', _ckey='dpErk_16.5hpf'):

    sbcadata = sbc_adata.copy()
    df = _pd.read_excel(_sbcmap, sheet_name=sheet_name)

    df = df.fillna('-')

    df.columns = df.columns.str.strip()

    ind1 = df[[_ckey, 'Index1']]
    ind1.columns = [_ckey, 'Gene ID']
    ind2 = df[[_ckey, 'Index2']]
    ind2.columns = [_ckey, 'Gene ID']

    df = _pd.concat([ind1, ind2])

    df = df.set_index('Gene ID')
    df.index = df.index.str.replace('Sam', 'S')

    df = sbcadata.var.merge(df, left_index=True, right_index=True)

    df[_ckey] = df[_ckey].astype(str)

    if returnmap:
        return df

    sbcadata.var[_ckey] = df[_ckey]
    sbcadata.var = sbcadata.var.set_index(_ckey)

    sbcadata = sbcadata.to_df().T
    sbcadata = sbcadata.groupby(sbcadata.index).sum().T
    return sbcadata
