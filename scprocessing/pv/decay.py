from scipy.sparse import issparse as _issparse
from sklearn.base import BaseEstimator as _BaseEstimator, TransformerMixin as _TransformerMixin
# from sklearn.linear_model import RANSACRegressor as _RANSACRegressor
from sklearn.linear_model import LinearRegression as _LinearRegression
from anndata import AnnData as _AnnData
import numpy as _np
import scipy as _sp
# import pandas as _pd
# import scanpy as _sc
import time as _time


class Decay(_BaseEstimator, _TransformerMixin):
    """Documentation for decay

    """
    def __init__(self,
                 regressmethod=_LinearRegression,
                 rolling_window_size=None,
                 quantile_window=100,
                 lower_quantile=0.2,
                 expression='X',
                 velocity='velocity',
                 memoryefficient=True,
                 subset_obs=None,
                 verbose=True,
                 activity=False,
                 add_key='decay',
                 regresskwargs={'fit_intercept': False, 'n_jobs': -1}):

        super(Decay, self).__init__()
        self.regressmethod = regressmethod
        self.rolling_window_size = rolling_window_size
        self.quantile_window = quantile_window
        self.lower_quantile = lower_quantile
        self.expression = expression
        self.velocity = velocity
        self.memoryefficient = memoryefficient
        self.subset_obs = subset_obs
        self.verbose = verbose
        self.regresskwargs = regresskwargs
        self.activity = activity
        self.add_key = add_key

    def _make_dense(self, X):

        if not self.memoryefficient and _issparse(X):
            X = X.A

        return X

    def _get_layer_data(self, data, copy=True):

        if isinstance(data, _AnnData):
            adata = data.copy() if copy else data
            if self.expression == 'X':
                X = adata.X
            elif self.expression == 'raw':
                X = adata.raw.X
            else:
                X = adata.layers[self.expression]

            Y = adata.layers[self.velocity]
        else:
            X = data.copy()
            Y = None

        X = self._make_dense(X)

        return X, Y

    def _iterate_quantile(self, y):

        qtls = _np.zeros(y.shape) * _np.nan
        halfwindow = self.quantile_window // 2
        for i in _np.arange(halfwindow, y.size - halfwindow, 1):
            ra = [i - halfwindow, i + halfwindow]
            qtls[i] = _np.quantile(y[ra[0]:ra[1]], self.lower_quantile)

        qtls[_np.isnan(qtls)] = _np.nanmax(qtls)

        return qtls

    def rolling_regression(self, x, y):

        rollwindow = y.size // 10 if self.rolling_window_size is None else self.rolling_window_size
        rollwindow = rollwindow // 2
        rollwindow = max(rollwindow, 3)

        alphas = []
        intercept = []
        for i in _np.arange(rollwindow, y.size - rollwindow - 1, 1):

            ra = [i - rollwindow, i + rollwindow]
            wx = x[ra[0]:ra[1]]
            wy = y[ra[0]:ra[1]]

            regf = self.regressmethod(**self.regresskwargs)

            regf = regf.fit(wx.reshape(-1, 1), _np.abs(wy))

            alphas.append(regf.coef_[0])

            if hasattr(regf, 'intercept_'):
                intercept.append(regf.intercept_)
            else:
                intercept.append(0.0)

        return alphas, intercept

    def _iterate_feature(self, X, Y):

        tot = X.shape[0]
        negatives = Y < 0

        decays = {}
        intercepts = {}
        start_time = _time.time()
        self._extime = start_time
        for i, (x, y, neg) in enumerate(zip(X, Y, negatives)):

            if self.verbose:
                print(f'Working on gene {i}/{tot} = {100*i/(tot-1):.3f}%, t = {self._extime:.3f}s', end='\r')

            if _np.sum(neg) < self.quantile_window:
                decays[i] = _sp.array([])
                self._extime = _time.time() - start_time
                continue

            x, y = self._extract_quantiles(x, y, neg)

            rolling_alpha, rolling_intercept = self.rolling_regression(x, y)

            decays[i] = _np.abs(_sp.array(rolling_alpha))
            intercepts[i] = _sp.array(rolling_intercept)

            self._extime = _time.time() - start_time

        return decays, intercepts

    def _extract_quantiles(self, x, y, negatives):

        x = x[negatives]
        y = y[negatives]

        indx = x.argsort()
        y = y[indx]
        x = x[indx]
        qtls = self._iterate_quantile(y)
        x = x[y < qtls]
        y = y[y < qtls]

        return x, y

    def get_best_decay(self, qt=0.95):

        opt_decay = []
        opt_intercept = []
        for k, v in self.decays_.items():
            if v.size == 0:
                opt_decay.append(0)
                opt_intercept.append(0)
            else:
                ic = self.intercepts_[k][v < _np.quantile(v, qt)]
                v = v[v < _np.quantile(v, qt)]
                if v.size == 0:
                    opt_decay.append(0)
                    opt_intercept.append(0)
                else:
                    opt_decay.append(v.max())
                    opt_intercept.append(ic[_np.argmax(v)])

        self.opt_decay = opt_decay
        self.opt_intercept = opt_intercept

    def fit(self, X, Y=None, copy=True):

        if isinstance(X, _AnnData):
            adata = X.copy() if copy else X

            X, Y = self._get_layer_data(adata)
        else:
            if Y is None:
                raise ValueError("Y needs to be set if X is a matrix")

        if self.activity:
            decays, intercepts = self._iterate_feature(X.T, -Y.T)
        else:
            decays, intercepts = self._iterate_feature(X.T, Y.T)

        self.decays_ = decays
        self.intercepts_ = intercepts

        return self

    def transform(self, X, Y=None, copy=False, qt=0.95):

        if isinstance(X, _AnnData):
            adata = X.copy() if copy else X

            # self.get_best_decay(qt=gt)
            opt_decay, opt_intercept = self.transform(adata.X, copy=True, qt=qt)

            if self.activity:
                adata.var['activity'] = opt_decay
                adata.var['activity_intercept'] = opt_intercept
            else:
                adata.var[self.add_key] = opt_decay
                adata.var[self.add_key + '_intercept'] = opt_intercept

            return adata if copy else None

        self.get_best_decay(qt=qt)
        return self.opt_decay, self.opt_intercept


def adjustPV2decay(X, velocity=None, decay=None, copy=False, l1='velocity', l2='X', decay_key='decay'):

    if isinstance(X, _AnnData):
        adata = X.copy() if copy else X

        if l2 == 'X':
            X = adata.X
        else:
            X = adata.layers[l2]

        velocity = adata.layers[l1]

        if decay_key not in adata.var_keys():
            raise ValueError(f"Can't find decay values in {decay_key}.")

        decay = adata.var[decay_key].values

    else:
        if (velocity is None) or (decay is None):
            raise ValueError("Need to supply velocity and decay rates as well.")

    adjvelocity = velocity + decay * X

    return adjvelocity


def convert2TranscriptionEffect(data, adj_rate_layer='adjPV', qt=0.95, add_layer='TFE', copy=False):

    data = data.copy() if copy else data
    X = data.layers[adj_rate_layer].copy()
    # X[X <= _np.sqrt(_np.abs(X).min())] = _np.sqrt(_np.abs(X).min())  # 1e-3
    # minx = X[X > 0].min()
    minx = _np.array([i[i > 0].min() for i in X.T])
    Xs = []
    for x, m in zip(X.T, minx):
        x[x < m] = m
        Xs.append(x)

    X = _np.array(Xs).T
    # X = _np.array([(lambda m, i: i[i < m] = m) for m, i in zip(minx, X.T)])
    # X[X < _np.sqrt(minx)] = _np.sqrt(minx)
    # print(X.min())
    # X[X <= 0] = _np.sqrt(_np.abs(X).min())  # 1e-3
    xi = _np.quantile(X, qt, axis=0)
    X = xi / X
    X = X - 1
    # X[X <= _np.sqrt(_np.abs(X).min())] = _np.sqrt(_np.abs(X).min())
    # minx = X[X > 0].min()
    # X[X < _np.sqrt(minx)] = _np.sqrt(minx)

    minx = _np.array([i[i > 0].min() for i in X.T])
    Xs = []
    for x, m in zip(X.T, minx):
        x[x < m] = m
        Xs.append(x)
    X = _np.array(Xs).T

    # print(X.min())
    data.layers[add_layer] = -_np.log(X)
    data.var['xi'] = xi

    return data if copy else None


def convertVariance(data, variance_key='variance', vel_key='adjPV', decay_key='decay'):

    adj_var = data.var[decay_key]**2 * data.var[variance_key]

    if vel_key == 'X':
        Xm = data.X.mean(0)
    else:
        Xm = data.layers[vel_key].mean(0)

    return adj_var / Xm**2
