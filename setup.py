from setuptools import setup

DISTNAME = 'singlecellgrnikit'
VERSION = '0.1.1'
DESCRIPTION = "A set of modules for simplifying single cell analysis."
# with open('README.rst') as f:
#     LONG_DESCRIPTION = f.read()
MAINTAINER = 'Andreas Tjärnberg'
MAINTAINER_EMAIL = 'andreas.tjarnberg@nyu.edu'
URL = 'https://gitlab.com/Xparx/single-cell-grni-toolkit'
# DOWNLOAD_URL = 'https://pypi.org/project/scikit-grni/#files'
# LICENSE = 'LGPL'


setup(name=DISTNAME,
      version=VERSION,
      description=DESCRIPTION,
      url=URL,
      author=MAINTAINER,
      author_email=MAINTAINER_EMAIL,
      # license=LICENSE,
      packages=['scprocessing'],
      python_requires='>=3.6',
      install_requires=[
          'numpy',
          'scipy',
          'pandas',
          'scanpy',
          'scvelo',
          'anndata',
      ],
      zip_safe=False)
